# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Pipedrive::Product do
  subject { described_class.new(api_token: 'token') }

  context 'with #entity_name' do
    subject { super().entity_name }

    it { is_expected.to eq('products') }
  end
end
