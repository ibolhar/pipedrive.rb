# frozen_string_literal: true

require 'logger'
require 'active_support/core_ext/hash'
require 'active_support/concern'
require 'active_support/inflector'

ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'person', 'persons'
end

require 'hashie'
require 'faraday'
require 'faraday_middleware'
require 'pipedrive/version'

module Pipedrive
  extend self
  attr_accessor :api_base_url, :api_proxy_url, :oauth_base_url, :api_token, :debug,
                :client_id, :client_secret, :redirect_uri, :oauth_state
  attr_writer :user_agent, :logger

  # ensures the setup only gets run once
  @_ran_once = false

  # `api_proxy_url` is used for OAuth api requests
  @api_proxy_url  = 'https://api-proxy.pipedrive.com'
  @api_base_url   = 'https://api.pipedrive.com'
  @oauth_base_url = 'https://oauth.pipedrive.com'

  def reset!
    @logger       = nil
    @_ran_once    = false
    @user_agent   = nil
    @api_token    = nil

    @client_id     = nil
    @client_secret = nil
    @redirect_uri  = nil
    @oauth_state   = nil
  end

  def user_agent
    @user_agent ||= "Pipedrive Ruby Client v#{::Pipedrive::VERSION}"
  end

  def setup
    yield self unless @_ran_once
    @_ran_once = true
  end

  def logger
    @logger ||= Logger.new(STDOUT)
  end

  reset!
end

require 'pipedrive/railties' if defined?(::Rails)

# Core
require 'pipedrive/base'
require 'pipedrive/utils'
require 'pipedrive/errors'
require 'pipedrive/operations/create'
require 'pipedrive/operations/read'
require 'pipedrive/operations/update'
require 'pipedrive/operations/delete'

# OAuth
require 'pipedrive/oauth'

# Persons
require 'pipedrive/person_field'
require 'pipedrive/person'

# Organizations
require 'pipedrive/organization_field'
require 'pipedrive/organization'

# Filters
require 'pipedrive/filter'

# Products
require 'pipedrive/product_field'
require 'pipedrive/product'

# Roles
require 'pipedrive/role'

# Stages
require 'pipedrive/stage'

# Goals
require 'pipedrive/goal'

# Activities
require 'pipedrive/activity'
require 'pipedrive/activity_type'

# Deals
require 'pipedrive/deal_field'
require 'pipedrive/deal'

# Files
require 'pipedrive/file'

# Notes
require 'pipedrive/note'

# Recents
require 'pipedrive/recents'

# Users
require 'pipedrive/user'

# Webhook
require 'pipedrive/webhook'

# SearchResults
require 'pipedrive/search_result'
