# frozen_string_literal: true

module Pipedrive
  class Recents < Base
    include ::Pipedrive::Operations::Read
    include ::Pipedrive::Utils
  end
end
