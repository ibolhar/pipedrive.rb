# frozen_string_literal: true

module Pipedrive
  class PipedriveError < StandardError
    # attr_reader :message
  end

  class ParameterError < PipedriveError
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def to_s
      "`#{name}` required param is missing"
    end
  end

  class APIError < PipedriveError
    attr_reader :code
    attr_reader :http_body
    attr_reader :http_headers

    def initialize(http_response)
      @http_body    = http_response.body
      @http_headers = http_response.headers
    end
  end

  class InvalidAPItokenError < APIError
    # Code: 401
  end

  class RateLimitError < APIError
    # Code: 429
  end

  module OAuth
    class OAuthError < PipedriveError
    end

    class AuthenticationError < OAuthError
    end

    class InvalidClientError < OAuthError
    end

    class InvalidGrantError < OAuthError
    end

    class InvalidRequestError < OAuthError
    end

    class InvalidScopeError < OAuthError
    end
  end
end
