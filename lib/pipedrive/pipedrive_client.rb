# frozen_string_literal: true

module Pipedrive
  class PipedriveClient
    attr_reader :connection

    def initialize(connection = nil)
      self.connection = connection || default_connection
    end

    private

    def default_connection
      Faraday.new do |f|
        f.use Faraday::Response::RaiseError
        f.use Faraday::Response::Logger ::Pipedrive.logger if ::Pipedrive.debug

        # Net::HTTP::Persistent doesn't seem to do well on Windows or JRuby,
        # so fall back to default there.
        if RUBY_PLATFORM == 'java' || Gem.win_platform?
          f.adapter :net_http
        else
          f.adapter :net_http_persistent
        end
      end
    end
  end
end
