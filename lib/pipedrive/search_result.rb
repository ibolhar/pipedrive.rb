# frozen_string_literal: true

module Pipedrive
  class SearchResult < Base
    include ::Pipedrive::Operations::Read
    include ::Pipedrive::Utils

    VALID_FIELD_TYPES = %w[dealField personField organizationField productField].freeze

    def entity_name
      'searchResults'
    end

    def find_by_field(*args)
      params = args.extract_options!
      params[:term] ||= args[0]

      validate_params(params)

      return to_enum(:find_by_field, params) unless block_given?

      follow_pagination(:make_api_call, [:get, 'find'], params) { |item| yield item }
    end

    private

    def validate_params(params)
      raise '`term` is missing'       unless params[:term]
      raise '`field_key` is missing'  unless params[:field_key]

      unless VALID_FIELD_TYPES.include?(params[:field_type])
        raise "`field_type` is invalid should be one of: #{VALID_FIELD_TYPES.join(', ')}"
      end
    end
  end
end
