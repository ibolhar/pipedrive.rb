# frozen_string_literal: true

module Pipedrive
  class ActivityField < Base
    include ::Pipedrive::Operations::Read

    def entity_name
      'activityFields'
    end
  end
end
