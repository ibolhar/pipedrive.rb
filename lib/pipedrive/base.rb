# frozen_string_literal: true

module Pipedrive
  class Base
    attr_reader :api_token, :access_token

    def initialize(api_token: ::Pipedrive.api_token, access_token: nil)
      raise '`api_token` or `access_token` should be set' if api_token.nil? && access_token.nil?

      @api_token    = api_token
      @access_token = access_token
    end

    def connection
      return self.class.oauth_connection(access_token).dup if access_token
      return self.class.connection.dup                     if api_token
    end

    def make_api_call(*args)
      params = args.extract_options!
      method = args[0]
      raise 'method param missing' unless method.present?

      url = build_url(args, params.delete(:fields_to_select))

      begin
        res = connection.__send__(method.to_sym, url, params)
      rescue Errno::ETIMEDOUT
        retry
      rescue Faraday::ParsingError
        sleep 5
        retry
      end

      process_response(res)
    end

    def build_url(args, fields_to_select = nil)
      url = if api_token
              "/v1/#{entity_name}"
            else
              entity_name
            end

      url += "/#{args[1]}" if args[1]

      if fields_to_select.is_a?(::Array) && !fields_to_select.empty?
        url += ":(#{fields_to_select.join(',')})"
      end

      url += "?api_token=#{api_token}" if api_token
      url
    end

    def process_response(res)
      if res.success?
        data = if res.body.is_a?(::Hashie::Mash)
                 res.body.merge(success: true)
               else
                 ::Hashie::Mash.new(success: true)
               end
        return data
      end

      failed_response(res)
    end

    def failed_response(res)
      failed_res = res.body.merge(success: false, not_authorized: false, failed: false,
                                  wrong_scope: false)

      case res.status
      when 401
        failed_res[:not_authorized] = true
      when 403
        failed_res[:wrong_scope] = true
      when 420
        failed_res[:failed] = true
      end

      failed_res
    end

    def entity_name
      self.class.name.split('::')[-1].downcase.pluralize
    end

    class << self
      # This method smells of :reek:TooManyStatements
      def connection
        faraday_options = { url:     Pipedrive.api_base_url,
                            headers: { user_agent: ::Pipedrive.user_agent } }

        @connection ||= Faraday.new(faraday_options) do |conn|
          conn.request :json

          conn.response :logger, ::Pipedrive.logger if ::Pipedrive.debug
          # conn.response :raise_error
          conn.response :mashify
          conn.response :json, content_type: /\bjson$/

          # Net::HTTP::Persistent doesn't seem to do well on Windows or JRuby,
          # so fall back to default there.
          if RUBY_PLATFORM == 'java' || Gem.win_platform?
            conn.adapter :net_http
          else
            conn.adapter :net_http_persistent
          end
        end
      end

      def oauth_connection(access_token)
        faraday_options = { url:     Pipedrive.api_proxy_url,
                            headers: { user_agent: ::Pipedrive.user_agent } }

        @oauth_connection = Faraday.new(faraday_options) do |conn|
          conn.request :json
          conn.request :oauth2, access_token, token_type: :bearer

          conn.response :logger, ::Pipedrive.logger if ::Pipedrive.debug
          # conn.response :raise_error
          conn.response :mashify
          conn.response :json, content_type: /\bjson$/

          # Net::HTTP::Persistent doesn't seem to do well on Windows or JRuby,
          # so fall back to default there.
          if RUBY_PLATFORM == 'java' || Gem.win_platform?
            conn.adapter :net_http
          else
            conn.adapter :net_http_persistent
          end
        end
      end
    end
  end
end
