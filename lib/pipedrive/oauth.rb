# frozen_string_literal: true

module Pipedrive
  module OAuth
    extend self

    AVAILABLE_SCOPES = %w[admin base users:read recents:read search:read
                          deals:read deals:full
                          mail:read mail:full
                          activities:read activities:full
                          contacts:read contacts:full
                          products:read products:full].freeze

    def oauth_post(url, params, headers: {})
      faraday_options = { headers: headers.merge(user_agent: ::Pipedrive.user_agent) }

      connection = Faraday.new(faraday_options) do |conn|
        conn.request :url_encoded

        conn.response :logger, ::Pipedrive.logger if ::Pipedrive.debug
        conn.response :mashify
        conn.response :json, content_type: /\bjson$/

        # Net::HTTP::Persistent doesn't seem to do well on Windows or JRuby,
        # so fall back to default there.
        if RUBY_PLATFORM == 'java' || Gem.win_platform?
          conn.adapter :net_http
        else
          conn.adapter :net_http_persistent
        end
      end

      connection.post(url, params)
    end

    def oauth_base_url(opts)
      opts[:oauth_base_url] || Pipedrive.oauth_base_url
    end

    def authorize_url(params = {}, opts = {})
      params[:client_id]     = params[:client_id]    || Pipedrive.client_id
      params[:redirect_uri]  = params[:redirect_uri] || Pipedrive.redirect_uri
      params[:response_type] = 'code'

      path  = '/oauth/authorize?'
      path += URI.encode_www_form(params)

      URI.join(oauth_base_url(opts), path).to_s
    end

    def create(code, params = {}, opts = {})
      params[:client_id]     = params[:client_id]     || Pipedrive.client_id
      params[:client_secret] = params[:client_secret] || Pipedrive.client_secret
      params[:redirect_uri]  = params[:redirect_uri]  || Pipedrive.redirect_uri
      params[:grant_type]    = 'authorization_code'
      params[:code]          = code

      path = '/oauth/token'
      url  = URI.join(oauth_base_url(opts), path)

      oauth_post(url, params)
    end

    def refresh_token(params, opts = {})
      raise AuthenticationError if params[:refresh_token].nil?

      params[:client_id]     = params[:client_id]     || Pipedrive.client_id
      params[:client_secret] = params[:client_secret] || Pipedrive.client_secret
      params[:grant_type]    = 'refresh_token'

      url = URI.join(oauth_base_url(opts), '/oauth/token')
      base64_auth = Base64.urlsafe_encode64(params[:client_id] + params[:client_secret])
      auth_header = { Authorization: "Basic <#{base64_auth}>" }

      oauth_post(url, params, headers: auth_header)
    end

    def revoke_token(params, opts = {})
      raise AuthenticationError if params[:token].nil?

      params[:client_id]     = params[:client_id]     || Pipedrive.client_id
      params[:client_secret] = params[:client_secret] || Pipedrive.client_secret

      url = URI.join(oauth_base_url(opts), '/oauth/revoke')
      base64_auth = Base64.urlsafe_encode64(params[:client_id] + params[:client_secret])
      auth_header = { Authorization: "Basic <#{base64_auth}>" }

      oauth_post(url, params, headers: auth_header)
    end
  end
end
